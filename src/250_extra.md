# Utilidades

## Utilidades variadas


gpick con

:    _Agave_ y _pdftk_ ya no existen, nos pasamos a _gpick_ y _poppler-utils_

     `sudo apt install gpick`

graphviz

:    Una utilidad de generación de gráficos que uso a veces. También
     es útil para `web2py` y para `org-roam

     `sudo apt install graphviz`

sqlite3

:    Un motor de bases de datos sencillo que se uso a menudo

     `sudo apt install sqlite3`

cheat

:    Chuletas de comandos habituales, se instala bajando el ejecutable
     desde [su github](https://github.com/cheat/cheat/releases)

cheat.sh

:    Echa una mirada a su página web: <http://cheat.sh/>, es casi
     idéntico al anterior pero disponible desde cualquier ordenador
     con conexión.

gparted

:    Instalamos _gparted_ para poder formatear memorias usb

     `sudo apt install gparted`

wkhtmltopdf

:    Para pasar páginas web a pdf

     `sudo apt install wkhtmltopdf`

lsd

:    `ls` potenciado, instalamos el paquete desde [la página de
     releases del github del
     proyecto](https://github.com/Peltoche/lsd/releases)

bat

:    `cat` potenciado, instalamos el paquete desde [la página de
     releases del github del
     proyecto](https://github.com/sharkdp/bat/releases)

nmap ndiff ncat

:    `nmap` nos permite realizar mapeos de subredes en nuestras redes
     locales. Por ejemplo para localizar dispositivos enganchados a
     nuestra red. `ndiff` nos permite comparar escaneos realizados con
     `nmap` y `ncat` hace todo tipo de cosas (mira en la red)

     `sudo apt install nmap ndiff ncat`
     
rofi
 
:    El super conmutador de ventanas (y muchas más cosas).

     Creamos el fichero `~/.config/rofi/config.rasi` con el siguiente contenido
     
     ```json
     configuration {
       modi: "drun,run,ssh,combi";
       theme: "fancy";
       font: "mensch 16";
       combi-modi: "window,drun,ssh";
     }
     ```
     
     Asociamos un atajo de teclado al comando: `rofi -show drun`

## time-tracking

### Activity Watcher

Instalado desde la web

En realidad no lo uso para nada.

### go for it

Este programa no para de escribir en el disco continuamente. He dejado
de usarlo por que me sobra con el org-mode de emacs.

Si de todas formas lo quieres instalar, aquí tienes los comandos:

~~~~{bash}
sudo add-apt-repository ppa:go-for-it-team/go-for-it-daily && sudo apt-get update
sudo apt-get install go-for-it
~~~~

## autokey

Instalamos [autokey](https://github.com/autokey/autokey) siguiendo [las instrucciones para instalarlo con paquetes debian](https://github.com/autokey/autokey/wiki/Installing#debian-and-derivatives)

Me he descargado la última estable en el Legion y la beta en el Acer.

Después de descargar los paquetes Debian ejecutamos los siguientes comandos (para la estable):

```bash
VERSION=0.95.10-0
sudo dpkg --install autokey-common_0.95.10-0_all.deb autokey-gtk_0.95.10-0_all.deb
sudo apt --fix-broken install
```

El único paquete que se instala a mayores en el python del sistema sería `python3-pyinotify`

Si estamos usando un entorno virtual con pyenv creo que sería mejor usar la instalación con `pip` ([ver documentación](https://github.com/autokey/autokey/wiki/Installing#pip-installation))

Para lanzar la aplicación ejecutamos: `python3 -m autokey.gtkui`


# Internet

## Rclone

Instalamos desde la página web(https://rclone.org/), descargando el
fichero `.deb`.

~~~~
curl https://rclone.org/install.sh | sudo bash
~~~~

### Recetas rclone

Copiar directorio local en la nube:

~~~~
rclone copy /localdir hubic:backup -vv
~~~~

Si queremos ver el directorio en la web de Hubic tenemos que copiarlo
en _default_:

~~~~
rclone copy /localdir hubic:default/backup -vv
~~~~

Sincronizar una carpeta remota en local:

~~~~
rclone sync hubic:directorio_remoto /home/salvari/directorio_local -vv
~~~~


### Referencias

* [Como usar rclone (blogdelazaro)](https://elblogdelazaro.gitlab.io//articles/rclone-sincroniza-ficheros-en-la-nube/)
* [y con cifrado (blogdelazaro)](https://elblogdelazaro.gitlab.io//articles/rclone-cifrado-de-ficheros-en-la-nube/)
* [Documentación](https://rclone.org/docs/)


## Palemoon ##

Un fork de _Firefox_ con menos chorradas. Instalado con el paquete `deb` descargado de su [página web](https://software.opensuse.org/download.html?project=home:stevenpusser&package=palemoon)

## LibreWolf ##

Otro fork de _Firefox_ centrado en la privacidad. Instalado como _appimage_ descargado desde su [página web](https://librewolf-community.gitlab.io/)

**UPDATE**: Ya está disponible el repo para Mint:

```bash
echo "deb [arch=amd64] http://deb.librewolf.net $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/librewolf.list
sudo wget http://deb.librewolf.net/keyring.gpg -O /etc/apt/trusted.gpg.d/librewolf.gpg
sudo apt update
sudo apt install librewolf -y
```

### Plugins instalados

Conviene estudiar la documentación de los _add-ons_ recomendados, disponible [aqui](https://librewolf-community.gitlab.io/docs/addons/).

* KeepassXC-Browser

    * Necesitamos instalar el KeepassXC, el que viene en los repos es un poco antiguo podemos instalar desde PPA
    * Es imprescindible hacer un link con `ln -s ~/.mozilla/native-messaging-hosts ~/.librewolf/native-messaging-hosts`
* Clear URLs
* Mozilla Multiaccount Containers

## Netsurf ##

Un navegador ultraligero (aunque no funciona con muchas páginas, solo para webs austeras) Instalado via flathub con `flatpak install netsurf`

## Lagrange ##

Un navegador para el protocolo _Gemini_. Instalado con la _appimage_ desde su [página web](https://git.skyjake.fi/gemini/lagrange)

## Castor ##

Otro navegador para el protocolo _Gemini_ programado en _Rust_. Instalado desde las fuentes siguiendo instrucciones de su [página web](https://sr.ht/~julienxx/Castor/)

## Sengi: Cliente de Mastodon

Instalada _appimage_ desde su [github](https://github.com/NicolasConstant/sengi)

# Window Managers adicionales

## i3wm

Añadimos el repo:

```bash
cd ~/tmp
/usr/lib/apt/apt-helper download-file https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2021.02.02_all.deb keyring.deb SHA256:cccfb1dd7d6b1b6a137bb96ea5b5eef18a0a4a6df1d6c0c37832025d2edaa710

sudo dpkg -i ./keyring.deb
sudo echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" >> /etc/apt/sources.list.d/sur5r-i3.list
sudo apt update
sudo apt install i3
``` 

## qtile

Vamos a describir la instalación del _Qtile_ en un _virtualenv_ dedicado. Si te bajas la versión estable de _Qtile_ desde su [página web](http://qtile.org) en el paquete vienen un montón de script auxiliares que te permiten hacer la instalación aislada en un _virtualenv_ pero lo voy a hacer a mano para tenerlo controlado con `pyenv`.

* Creamos el _virtualenv_ `qtile` basado en la versión 3.10.0 que teniamos instalada previamente:

```bash
# Creamos el directorio de trabajo
mkdir <workPath>/qtile
cd <workPath>/qtile

# OPCIONAL: Descargamos el qtile estable de la página web
# sólo si quieres curiosear los scripts auxiliares
wget https://github.com/qtile/qtile/archive/v0.18.1.tar.gz
tar xvzf v0.18.1.tar.gz
rm v0.18.1.tar.gz

# Creamos el entorno virtual
pyenv virtualenv 3.10.0 qtile
pyenv local qtile
# Instalamos los paquetes iniciales (comunes a mis entornos)
pip install --upgrade pip setuptools wheel

# instalamos los requisitos
pip install --no-cache-dir xcffib
pip install --no-cache-dir cairocffi

# Instalamos la versión estable de qtile
pip install --no-cache-dir qtile
```

Con esto ya estamos listos, podríamos arrancar _Qtile_ con `qtile start`, pero no puede funcionar claro. Para que arranque correctamente, tenemos que lanzarlo en un servidor X. (ver el punto "[Lanzar Window Managers con Xephyr]")

### Configurar Qtile en _virtualenv_ para arrancer desde el _Lightdm_

Como `root` nos creamos un script `launch_qtile` en `/usr/local/bin`, con el siguiente contenido

```bash
#!/bin/bash
source '/home/user/.pyenv/versions/3.10.0/envs/qtile/bin/activate'
qtile start
```

Le damos permisos de ejecución con `chmod 755 launch_qtile` (ojo a los permisos para _all_ que si no son estos no le gusta a _Lightdm_)

También como `root` creamos el fichero `/usr/share/xsessions/qtile.desktop` con el contenido:

```bash
[Desktop Entry]
Name=Qtile
Comment=Qtile Session
Exec=launch_qtile
Type=Application
Keywords=wm;tiling
```

Y con esto tendremos Qtile disponible en _Lightdm_.

## Lanzar Window Managers con Xephyr

Para probar (o configurar) los _Window Managers_ sin salir de nuestra sesión de Mate podemos usar Xephyr, si no lo tienes instalado ejecuta:

```bash
sudo apt update
sudo apt install xserver-xephyr
```

Para lanzar un _Xserver_ usaríamos un comando como:

```bash
Xephyr -ac -screen 800x600 -br -reset -terminate 2> /dev/null :1 &
```

**-ac**

:    Autorizar conexiones de clientes indiscriminadamente (_disable access restrictions_)

**-screen**

:    Especificar la geometría de la pantalla.

**-br**

:    La ventana raiz tendrá fondo negro

**-reset**

:    Reset al terminar el último cliente

**-terminate**

:    Finalizar cuando se resetee el servidor

**2> /dev/null** 

:    Mandar los mensajes de error al limbo (alias **NE** en nuestro pc)

**:1**

:    Arrancar el server en el DISPLAY=1

Asi que si queremos arrancar por ejemplo el _i3wm_ podríamos hacer un script con las siguientes lineas:

```bash
Xephyr -ac -screen 800x600 -br -reset -terminate 2> /dev/null :1 &
export DISPLAY=:1
i3
```

Para _Qtile_ bastaria con cambiar `i3` por `qtile start`


# Comunicación con dispositivos Android

## scrcpy

`sudo apt install scrcpy`

## Heimdall

Para flashear roms en moviles

```bash
sudo apt install heimdall-flash heimdall-flash-frontend
```

