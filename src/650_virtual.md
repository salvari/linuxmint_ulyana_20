# Virtualizaciones y contenedores

## Instalación de _virtualBox_

__AVISO IMPORTANTE__

Tenemos dos formas de instalar _Virtualbox_, desde los repos oficiales de la aplicación o desde los propios de Linux Mint (Ubuntu Focal Fossa)

Si descargamos los oficiales de _Virtualbox_ se instalará el paquete `python-is-python2`, eso hara que el python por defecto de nuestro sistema sea el dos. A cambio tendremos la última versión de _Virtualbox_

Si lo instalamos con los repos de Ubuntu, podemos tener instalado el paquete `python-is-python3` (esta es mi opción favorita)

### Instalación desde Ubuntu

```bash
sudo apt install virtualbox virtualbox-ext-pack virtualbox-guest-addition-iso
```


### Instalación desde repos oficiales

Lo hacemos con los origenes de software oficiales (alternativamente, podríamos hacerlo manualmente):

~~~~
# Importamos la clave gpg
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

# Añadimos el nuevo origen de software
sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(. /etc/os-release; echo "$UBUNTU_CODENAME") contrib"

# Actualizamos la base de datos de paquetes
sudo apt update
~~~~

Ahora podemos comprobar que además del paquete _virtualbox_ tenemos
varios paquetes con número de versión (p.ej. _virtualbox.6.1), estos
últimos son los que hemos añadido (compruebalo con `apt-cache policy
[nombrepaquete]`)

Instalamos el que nos interesa:

~~~~{bash}
sudo apt install virtualbox-6.1
~~~~

**ATENCIÓN**

~~~~
The following additional packages will be installed:
  python-is-python2
~~~~

Descargamos también el [VirtualBox Extension
Pack](https://www.virtualbox.org/wiki/Downloads), este paquete lo
podemos instalar desde el propio interfaz de usuario del _VirtualBox_,
o bien con el siguiente comando:

~~~~
sudo VBoxManage extpack install ./Oracle_VM_VirtualBox_Extension_Pack-6.1.2.vbox-extpack
~~~~


Sólo nos queda añadir nuestro usuario al grupo `vboxusers`, con el
comando `sudo gpasswd -a username vboxusers`, y tendremos que cerrar
la sesión para refrescar nuestros grupos.


## qemu

Un par de comprobaciones previas:

* El comando `egrep -c '(vmx|svm)' /proc/cpuinfo` debe devolvernos un
  número mayor que cero si nuestro sistema soporta virtualización.
* El comando `kvm-ok` nos sirve para comprobar que la virtualización
  hardware no está deshabilitada en la BIOS (puede que tengas que
  ejecutar `apt install cpu-checker`)

Instalamos desde el repo oficial:

~~~~
sudo apt install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virtinst virt-manager
sudo apt install virt-viewer
~~~~

qemu-kvm

:    nos da la emulación hardware para el hipervisor KVM

libvirt-daemon-system

:    los ficheros de configuración para ejecutar el demonio `libvirt`
     como servicio

libvirt-clients

:    software para gestionar plataformas de virtualización

bridge-utils

:    utilidades de linea de comandos para configurar bridges ethernet

virtinst

:    utilidades de linea de comandos para crear máquinas virtuales

virt-manager

:    un interfaz gráfico junto con utilidades de linea de comandos
     para gestionar máquinas virtuales a través de _libvirt_

Solo queda añadir nuestro usuario a los grupos:

~~~~
sudo gpasswd -a username libvirt
sudo gpasswd -a username kvm
~~~~

Podemos comprobar el estado del servicio con `scs libvirtd`
(`systemctl status libvirtd`).

### Referencias

* [How to install KVM on Ubuntu 20.04 Graphical & headless
  server](https://www.how2shout.com/how-to/how-to-install-kvm-on-ubuntu-20-04-graphical-headless-server.html)
* [How to Install Kvm on Ubuntu 20.04](https://linuxize.com/post/how-to-install-kvm-on-ubuntu-20-04/)
* [How to Install KVM on Ubuntu 20.04](https://www.tecmint.com/install-kvm-on-ubuntu/)


## Docker

Tenemos que añadir el repositorio correspondiente a nuestra
distribución:

~~~~
# Be safe
sudo apt remove docker docker-engine docker.io
sudo apt autoremove
sudo apt update

# Install pre-requisites
sudo apt install ca-certificates curl

# Import the GPG key

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Next, point the package manager to the official Docker repository

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"

# Update the package database

sudo apt update
#

apt-cache policy docker-ce

sudo apt install docker-ce

sudo gpasswd -a username docker
~~~~

Esto dejará el servicio _docker_ funcionando y habilitado (arrancará
en cada reinicio del ordenador)

La forma de pararlo es:

~~~~{bash}
sudo systemctl stop docker
sudo systemctl disable docker
systemctl status docker
~~~~

Añadimos el _bundle_ __docker__ en nuestro fichero `~/.zshrc` para
tener autocompletado en comandos de docker.

Para usar _docker_ tendremos que arrancarlo, con los alias de nuestro sistema para _systemd_ ejecutamos:

~~~~{bash}
scst docker  # para arrancar el servicio

scsp docker  # para parar el servicio
~~~~

### docker-compose

* Nos bajamos la última versión disponible de [las releases de
  github](https://github.com/docker/compose/releases)
* Movemos el fichero que hemos descargado a `/usr/local/bin/docker-compose`
* Y le damos permisos de ejecución `sudo chmod +x /usr/local/bin/docker-compose`

### Kitematic

Un interfaz gráfico para _Docker_. En su [página de
releases](https://github.com/docker/kitematic/releases) bajamos la
última para Ubuntu e instalamos con el gestor de paquetes.

La verdad es que me gusta más el CLI.
