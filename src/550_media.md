# Sonido

## Spotify

Spotify instalado desde las opciones de Linux Mint via flatpak.

## Audacity

El ppa de Audacity no permite instalar la última versión. Podemos
instalarla via flatpak o simplemente instalar la de los repos (no es
la última)

Es de esperar que al final la añadan al ppa asi que dejamos aquí las instrucciones.

Añadimos ppa:

~~~~
sudo add-apt-repository ppa:ubuntuhandbook1/audacity
sudo apt-get update
sudo apt install audacity
~~~~

Instalamos también el plugin [Chris’s Dynamic Compressor
plugin](https://theaudacitytopodcast.com/chriss-dynamic-compressor-plugin-for-audacity/)

## Clementine

La version disponible en los orígenes de software parece al dia:

~~~~
sudo apt install clementine
~~~~

# Video

## Shotcut

Nos bajamos la _AppImage_ para Linux desde la [página
web](https://www.shotcut.org/).

La dejamos en `~/apps/shotcut` y:

~~~~
cd
chmod 744 Shotcutxxxxxx.AppImage
./Shotcutxxxxxx.AppImage
~~~~

## kdenlive

Está disponible [en la web](https://kdenlive.org) como ppa o como _appimage_. Lo he bajado como _appimage_ para probarlo.

## Openshot

También descargado desde [su web](https://www.openshot.org) como _appimage_, para probar. Tienen un ppa disponible.

## Avidemux

Descargado _appimage_ desde [la web](http://avidemux.sourceforge.net/)

## Handbrake

Instalado como flatpak desde [su web](https://handbrake.fr/).

## Grabación de screencast

### Vokoscreen, Kazam y SimpleScreenRecorder

Instalados desde los repos oficiales:

~~~~
sudo apt update
sudo apt install vokoscreen  vokoscreen-ng kazam simplescreenrecorder
~~~~

Escoge el que más te guste.

### OBS

Añadimos el repositorio

~~~~{bash}
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt update
sudo apt install obs-studio
~~~~

## Grabación de podcast

### Mumble

Podemos instalarlo desde flatpak o bajarnos [el paquete
antiguo](https://launchpad.net/~mumble/+archive/ubuntu/release/+packages)
(parece que funciona bien).

Mumble no está disponible desde el PPA, aunque dejo aquí las
instrucciones por si lo corrigen.

~~~~
sudo add-apt-repository ppa:mumble/release
sudo apt update
    sudo apt install mumble
~~~~

## Clientes de youtube

### smtube

Instalado el ppa siguiendo instrucciones de [su página web](http://www.smtube.org/).

```bash
sudo add-apt-repository ppa:rvm/smplayer
sudo apt-get update
sudo apt-get install smtube
```

### Freetube

Descargado el `.deb` desde [su página web](https://freetubeapp.io/#download).

# Fotografía

## Rawtherapee

Bajamos el AppImage desde la [página web](http://rawtherapee.com/) al
directorio `~/apps/rawtherapee`.

~~~~
cd
chmod 744 RawTherapeexxxxxx.AppImage
./RawTherapeexxxxxx.AppImage
~~~~

Al ejecutarla la primera vez ya se encarga la propia aplicación de
integrarse en nuestro sistema.


## Darktable

Instalamos ppa (ver [esta web](https://software.opensuse.org/download/package?package=darktable&project=graphics%3Adarktable))

~~~~
echo 'deb http://download.opensuse.org/repositories/graphics:/darktable/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/graphics:darktable.list
curl -fsSL https://download.opensuse.org/repositories/graphics:darktable/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/graphics:darktable.gpg > /dev/null
sudo apt update
sudo apt install darktable
~~~~


Se instala la última versión de Darktable (3.0.2)

__OJO__: Conviene renombrar el fichero de claves de darktable, a
nuestro linux no le gustan los ficheros con un ':' Revisa
`/etc/apt/trusted.gpg.d/`


## Digikam

Instalado desde la [página web](https://www.digikam.org/) de la
aplicación con appimage.

## Webcamoid

Descargada la appimage desde la [página web](https://webcamoid.github.io/)
