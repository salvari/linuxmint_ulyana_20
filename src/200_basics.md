# Programas básicos

## Linux Mint

Linux Mint incluye `sudo` y las aplicaciones que uso habitualmente
para gestión de paquetes por defecto (_aptitude_ y _synaptic_).

Interesa tener instalado el paquete _ppa-purge_ (`sudo apt install
ppa-purge`). Sirve para eliminar ppas junto con los programas
instalados desde ese ppa.

Tampoco voy a enredar nada con los orígenes del software (de momento),
es decir no voy a cambiar al depósito regional. Si quieres cambiarlo,
en mi experiencia los más rápidos suelen ser los alemanes.

## Firmware

Ya no es necesario intalar los paquetes de _microcode_ la instalación
de Una se encargó de instalar:

* `amd64-microcode`
* `intel-microcode`

Instalamos el driver de nvidia recomendado, después de la instalación
inicial el _Mint_ nos avisará de que tenemos que revisar la
instalación de los drivers.

El driver de Nvidia viene muy mejorado. Merece la pena ver todas las
opciones.

Una vez instalado el driver de Nvidia, el comando `prime-select
query`debe indicarnos la tarjeta activa y podremos cambiar de tarjeta
ejecutando `prime-select [nvidia|intel]`. También podremos acceder a
las funciones de Nvidia a través del applet en la barra de estado de
nuestro escritorio.

## Control de configuraciones con git

Una vez instalado el driver de Nvidia y antes de seguir con la
instalación instalamos el `git` y el `etckeeper` para que todos los
cambios que se produzcan en el directorio `/etc` durante nuestra
instalación queden reflejados en el git.

Yo nunca almaceno esta información en la nube (por seguridad), pero me
permite tener controlados los cambios de configuración y ayuda en caso
de problemas.

### Instalación de `etckeeper`

¡Ojo!, nos hacemos `root` para ejecutar:

~~~~{bash}
sudo su -
git config --global user.email xxxxx@whatever.com
git config --global user.name "Name Surname"
apt install etckeeper
~~~~

_etckeeper_ hara un control automático de tus ficheros de
configuración en `/etc`

Para echar una mirada a los _commits_ creados puedes ejecutar:

~~~~{bash}
cd /etc
sudo git log
~~~~

### Controlar dotfiles con git

Vamos a crear un repo de git para controlar nuestros ficheros
personales de configuración.

Creamos el repo donde queramos, yo suelo usar el directorio `~/work/repos`.

~~~~{bash}
mkdir usrcfg
cd usrcfg
git init
git config core.worktree "/home/salvari"
~~~~

Y ya lo tenemos, un repo de git, en la localización que queramos y que
tiene el directorio de trabajo apuntando a nuestro _$HOME_.

Podemos añadir los ficheros de configuración que queramos al repo:

~~~~{bash}
git add .bashrc
git commit -m "Add some dotfiles"
~~~~

Una vez que tenga añadidos los ficheros que quiero tener controlados
pondré `*` en el fichero `.git/info/exclude` de mi repo para que
ignore todos los ficheros de mi `$HOME`.

Cuando instalo algún programa nuevo añado a mano los ficheros de
configuración que quiero tener controlados al repo.

Yo no tengo información confidencial en este repositorio (claves ssh
por ejemplo) así que no tengo problemas en almacenarlo en la nube.
Facilita mucho las cosas en casos de upgrade del sistema o copiar
configuraciones entre ordenadores.

## Editor desde linea de comandos

Hasta que instalemos nuestro editor favorito (_Emacs_ en mi caso) podemos usar _nano_ desde la linea de comandos para editar cualquier fichero. Pero yo añado _micro_ con el paquete `.deb` descargado desde [su github](https://github.com/zyedidia/micro/releases).

## Parámetros de disco duro

Tengo un disco duro ssd y otro hdd normal.

El area de intercambio la hemos creado en el disco duro hdd, no se
usará mucho (mejor dicho: no se usará nunca) pero evitamos multiples
operaciones de escritura en el disco ssd en caso de que se empiece a
tirar del swap.

Añadimos el parámetro `noatime` para las particiones de `root` y
`/home`, que si que se han creado en el ssd.

~~~~{fstab}
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda5 during installation
UUID=d96a5501-75b9-4a25-8ecb-c84cd4a3fff5 /               ext4    noatime,errors=remount-ro 0       1
# /home was on /dev/sda7 during installation
UUID=8fcde9c5-d694-4417-adc0-8dc229299f4c /home           ext4    defaults,noatime        0       2
# /store was on /dev/sdc7 during installation
UUID=0f0892e0-9183-48bd-aab4-9014dc1bd03a /store          ext4    defaults        0       2
# swap was on /dev/sda6 during installation
UUID=ce11ccb0-a67d-4e8b-9456-f49a52974160 none            swap    sw              0       0
# swap was on /dev/sdc5 during installation
UUID=11090d84-ce98-40e2-b7be-dce3f841d7b4 none            swap    sw              0       0
~~~~

Una vez modificado el `/etc/fstab` no hace falta arrancar, basta con
ejecutar lo siguiente:

~~~~{bash}
mount -o remount /
mount -o remount /home
mount
~~~~

### Ajustes adicionales para _Firefox_

Las diferencias de rendimiento del Firefox con estos ajustes son bastante notables.

Seguimos [esta referencia](https://easylinuxtipsproject.blogspot.com/p/ssd.html#ID10)

Visitamos `about::config` con el navegador.

Cambiamos

* `browser.cache.disk.enable` __false__
* `browser.cache.memory.enable` __true__
* `browser.cache.memory.capacity` __204800__
* `browser.sessionstore.interval` __15000000__

TODO: Comprobar _trim_ en mi disco duro. Y mirar
[esto](https://easylinuxtipsproject.blogspot.com/p/speed-mint.html)

Y siguiendo [esta otra referencia](https://linuxreviews.org/HOWTO_Make_Mozilla_Firefox_Blazing_Fast_On_Linux) cambiamos:

* `gfx.x11-egl.force-enabled` __true__
* **IMPORTANTE** Ajustar el parámetro `network.IDN_show_punycode` a __true__ (para evitar ataques de URL falsas con caracteres Unicode)

Reiniciamos nuestro _Firefox_ (podemos visitar `about:restartRequired`)

## Fuentes (tipográficas) adicionales

Instalamos algunas fuentes desde los orígenes de software:

~~~~{bash}
sudo apt install ttf-mscorefonts-installer
sudo apt install fonts-noto
~~~~

Y la fuente
[Mensch](https://robey.lag.net/2010/06/21/mensch-font.html) la bajamos
directamente al directorio `~/.local/share/fonts`

Puede ser conveniente instalar el paquete _font-manager_ (`sudo apt
install font-manager`), sólo lo uso para visualizar fuentes, no para
hacer configuración.

Instaladas varias de las [nerd-fonts](https://github.com/ryanoasis/nerd-fonts) en `~/.local/share/fonts`. Simplemente descargamos las fuentes interesantes y ejecutamos `sudo fc-cache -f -v`

Fuentes interesantes:

* [nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
* [Powerline fonts](https://github.com/powerline/fonts)
* [Programming fonts](https://github.com/ProgrammingFonts/ProgrammingFonts)

## Firewall

`ufw` y `gufw` vienen instalados por defecto, pero no activados.

~~~~{bash}
aptitude install ufw
ufw default deny
ufw enable
ufw status verbose
aptitude install gufw
~~~~

-------------------------------

> __Nota__: Ojo con el log de `ufw`, tampoco le sienta muy bien al ssd
> esa escritura masiva. Yo normalmente lo dejo desactivado excepto
> cuando valido una nueva configuración.

-------------------------------


## Aplicaciones variadas

> __Nota__: Ya no instalamos _menulibre_, Linux Mint tiene una utilidad
> de edición de menús.

KeepassXC

:    Para mantener nuestras contraseñas a buen recaudo

Gnucash

:    Programa de contabilidad, la versión de los repos está bastante
     atrasada habrá que probar la nueva que puede instalarse desde la
     web o desde el flathub.

Deluge

:    Programa de descarga de torrents (acuérdate de configurar tus
     cortafuegos)

rsync, grsync

:    Para hacer backups de nuestros ficheros

Descompresores variados

:    Para lidiar con los distintos formatos de ficheros comprimidos

mc
:    Midnight Comander, gestor de ficheros en modo texto

most
:    Un `less` mejorado

tree
:    Para ver estructuras de directorios

neofetch
:    Este solo vale para presumir de ordenador creo ¬_¬

fasd
:    Para cambiar entre directorios rápidamente

silversearcher-ag
:    Una alternativa mejorada al clásico _grep_

ack
:    Otro grep mejorado

ncdu
:    Un analizador de uso de disco alternativa a `du`

mate-tweak
:    Para _customizar_ nuestro escritorio Mate

filezilla
:    Un interfaz gráfico para transferencia de ficheros

rofi
:    Un conmutador de ventanas capaz de automatizar muchas tareas

Chromium

:    Como Chrome pero libre, en Linux Mint no hay snaps, puedes instalarlo directamente con `apt`

~~~~
sudo apt install keepassxc gnucash deluge rsync grsync rar unrar \
zip unzip unace bzip2 lzop p7zip p7zip-full p7zip-rar \
most mc tree neofetch fasd silversearcher-ag ack ncdu mate-tweak filezilla \
rofi chromium
~~~~

## Algunos programas de control del sistema

Son útiles para control de consumo.

~~~~
sudo apt install tlp tlp-rdw htop powertop
~~~~

## Programas de terminal

Dos imprescindibles:

~~~~{bash}
sudo apt install guake terminator
~~~~

_terminator_ lo dejamos como aplicación terminal preferida del sistema.

__TODO:__ asociar _Guake_ a una combinación apropiada de teclas.

También instalo _rxvt_ para tener una alternativa ligera al _terminator_.

~~~~{bash}
sudo apt install rxvt-unicode
~~~~

### tmux

_tmux_ combinado por ejemplo con _rxvt_ nos da la misma funcionalidad que _Terminator_, además merece la pena aprender a usarlo por que instalado en servidores remotos es increíblemente útil.

~~~~{bash}
sudo apt install tmux
~~~~

- [El tao de tmux](https://leanpub.com/the-tao-of-tmux/read)
- [rxvt customizations](https://www.askapache.com/linux/rxvt-xresources/)


## Dropbox

Lo instalamos desde el software manager.

## Chrome

No lo he instalado.

Puede instalarse desde [la página web de
Chrome](https://www.google.com/chrome/)


## Varias aplicaciones instaladas de binarios

Lo recomendable en un sistema POSIX es instalar los programas
adicionales en `/usr/local` o en `/opt`. Yo soy más chapuzas y suelo
instalar en `~/apt` por que el portátil es personal e
intrasferible. En un ordenador compartido es mejor usar `/opt`.

En general cuando instalo en el directorio `~/apps` sigo los
siguientes pasos:

1. Descargamos los binarios o _appimage_ desde la web
2. Descomprimo en un nuevo directorio para la aplicación, tomamos como
   ejemplo freeplane, así que el directorio se llamará:
   `~/apps/mi_aplicacion`
3. Creamos enlace simbólico al que llamamos `current`. Esto es para no
   editar los ficheros `.desktop` cada vez que actualicemos la versión
   del programa. El enlace puede apuntar a un directorio o a un
   binario, depende de como obtengamos la aplicación.
   En el caso de freeplane yo tengo la siguiente estructura

    ~~~~
    freeplane
    ├── current -> freeplane-1.7.11
    ├── freeplane-1.7.10
    └── freeplane-1.7.11
    ~~~~

    Vemos que el enlace apunta a la versión más reciente de
    _freeplane_.
4. Añadimos la aplicación a los menús, al hacer esto se creará un fichero `.desktop` en el directorio `~/.local/share/applications`

### Freeplane

Para hacer mapas mentales, presentaciones, resúmenes, apuntes...  La
versión incluida en LinuxMint está un poco anticuada.

### Treesheets

Está bien para hacer chuletas rápidamente. Descargamos el _appimage_
desde [la web](http://strlen.com/treesheets/)

### Telegram Desktop

Cliente de Telegram, descargado desde la [página
web](https://desktop.telegram.org/). El programa de instalación de
Telegram ya se encarga de crear el fichero `.desktop`

### Tor browser

Descargamos desde la [página oficial del
proyecto](https://www.torproject.org/) Descomprimimos en `~/apps/` y
ejecutamos desde terminal:

~~~~
cd ~/apps/tor-browser
./start-tor-browser.desktop --register-app
~~~~

Tor se encarga tanto de crear el fichero `.desktop` como de mantenerse
actualizado a la última versión.

### Brave browser

Instalamos siguiendo las instrucciones de la [página web
oficial](https://brave-browser.readthedocs.io/en/latest/installing-brave.html#linux)

~~~~{bash}
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update

sudo apt install brave-browser
~~~~

### TiddlyDesktop

Descargamos desde la [página
web](https://github.com/Jermolene/TiddlyDesktop), descomprimimos y
generamos la entrada en el menú.

### Joplin

Una herramienta libre para mantener notas sincronizadas entre el móvil
y el portátil.

Instalamos siguiendo las instrucciones de la [página
web](https://joplinapp.org/)

~~~~{bash}
 wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash
~~~~

Joplin se instala en el directorio `~/.joplin` y crea su propia entrada en el menú.


## Terminal y shells

Por defecto tenemos instalado `bash`.


### bash-git-promt

Para dejar configurado el _bash-git-prompt_ seguimos las instrucciones
de [este github](https://github.com/magicmonty/bash-git-prompt)

### zsh

Nos adelantamos a los acontecimientos, pero conviene tener instaladas
las herramientas de entornos virtuales de python antes de instalar
_zsh_ con el plugin para _virtualenvwrapper_.

~~~~
apt install python-all-dev
apt install python3-all-dev
apt install virtualenv virtualenvwrapper python3-virtualenv
~~~~

_zsh_ viene por defecto en mi instalación, en caso contrario:

~~~~
apt install zsh
~~~~

Para _zsh_ vamos a usar
[antigen](https://github.com/zsh-users/antigen), así que nos lo
clonamos en `~/apps/`

~~~~
cd ~/apps
git clone https://github.com/zsh-users/antigen
~~~~

También vamos a usar
[zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt),
así que lo clonamos también:

~~~~
cd ~/apps
git clone https://github.com/olivierverdier/zsh-git-prompt
~~~~

Y editamos el fichero `~/.zshrc` para que contenga:

~~~~
# This line loads .profile, it's experimental
[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'

source ~/apps/zsh-git-prompt/zshrc.sh
source ~/apps/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle command-not-found

# must install autojump for this
#antigen bundle autojump

# extracts every kind of compressed file
antigen bundle extract

# jump to dir used frequently
antigen bundle z

#antigen bundle pip

antigen bundle common-aliases

antigen bundle robbyrussell/oh-my-zsh plugins/virtualenvwrapper

antigen bundle zsh-users/zsh-completions

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search ./zsh-history-substring-search.zsh

# Arialdo Martini git needs awesome terminal font
#antigen bundle arialdomartini/oh-my-git
#antigen theme arialdomartini/oh-my-git-themes oppa-lana-style

# autosuggestions
antigen bundle tarruda/zsh-autosuggestions

#antigen theme agnoster
antigen theme gnzh

# Tell antigen that you're done.
antigen apply

# Correct rm alias from common-alias bundle
unalias rm
alias rmi='rm -i'
~~~~

Para usar _virtualenvwrapper_ hay que decidir en que directorio
queremos salvar los entornos virtuales. El obvio seria
`~/.virtualenvs` la alternativa sería `~/.local/share/virtualenvs`.

El que escojamos lo tenemos que crear y añadirlo a nuestro
`~/.profile` con las líneas:

~~~~
# WORKON_HOME for virtualenvwrapper
if [ -d "$HOME/.virtualenvs" ] ; then
    WORKON_HOME="$HOME/.virtualenvs"
fi
~~~~

Después de seguir estos pasos basta con arrancar el _zsh_

_Antigen_ ya se encarga de descargar todos los plugins que queramos
utilizar en zsh. Todos el software se descarga en `~/.antigen`

Para configurar el
[zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt),
que inspiró el bash-git-prompt.

He modificado el fichero `zshrc.sh` de `zsh-git-prompt` cambiando la
linea `echo "$STATUS":

~~~~
#echo "$STATUS"
if [[ "$__CURRENT_GIT_STATUS" == ": 0 0 0 0 0 0" ]]; then
    echo ""
else
    echo "$STATUS"
fi
~~~~

También he cambiado el fichero del tema _gnzh_ en
`~/.antigen/bundles/robbyrussell/oh-my-zsh/themes/gnzh.zsh-theme` por
que me interesa ver la versión python asociada a cada virtualenv.

## Codecs

~~~~
sudo apt-get install mint-meta-codecs
~~~~

## Syncthing

Añadimos el ppa:

~~~~{bash}
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt-get update
sudo apt-get install syncthing
~~~~
