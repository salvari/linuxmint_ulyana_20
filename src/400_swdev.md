# Desarrollo software

## Paquetes esenciales

Estos son los paquetes esenciales para empezar a desarrollar software
en Linux.

~~~~
sudo apt install build-essential checkinstall make automake cmake autoconf \
git git-core git-crypt dpkg wget
~~~~

## Git

------

__NOTA__: Si quieres instalar la última versión de git, los git
developers tienen un ppa para ubuntu, si quieres tener el git más
actualizado:

~~~{bash}
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt upgrade
~~~

------

Control de versiones distribuido. Imprescindible. Para _Linux Mint_
viene instalado por defecto.

Configuración básica de git:

~~~~
git config --global ui.color auto
git config --global user.name "Pepito Pérez"
git config --global user.email "pperez@mikasa.com"

git config --global alias.cl clone

git config --global alias.st "status -sb"
git config --global alias.last "log -1 --stat"
git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %Cblue<%an>%Creset' --abbrev-commit --date=relative --all"
git config --global alias.dc "diff --cached"

git config --global alias.unstage "reset HEAD --"

git config --global alias.ci commit
git config --global alias.ca "commit -a"

git config --global alias.ri "rebase -i"
git config --global alias.ria "rebase -i --autosquash"
git config --global alias.fix "commit --fixup"
git config --global alias.squ "commit --squash"

git config --global alias.cp cherry-pick
git config --global alias.co checkout
git config --global alias.br branch
git config --global core.editor emacs
~~~~

## Emacs

Instalado emacs desde los repos:

~~~~
sudo aptitude install emacs
~~~~

## Lenguaje de programación D (D programming language)

El lenguaje de programación D es un lenguaje de programación de
sistemas con una sintaxis similar a la de C y con tipado estático.
Combina eficiencia, control y potencia de modelado con seguridad y
productividad.

### D-apt e instalación de programas

Configurado _d-apt_, instalados todos los programas incluidos

~~~~
sudo wget http://master.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  EBCF975E5BA24D5E
sudo apt update
~~~~

Instalamos todos los programas asociados excepto _textadept_ que no
uso.

~~~~
sudo apt install dmd-compiler dmd-tools dub dcd dfix dfmt dscanner
~~~~

### DCD

Una vez instalado el DCD tenemos que configurarlo creando el fichero
`~/.config/dcd/dcd.conf` con el siguiente contenido:

~~~~
/usr/include/dmd/druntime/import
/usr/include/dmd/phobos
~~~~

Podemos probarlo con:

~~~~
dcd-server &
echo | dcd-client --search toImpl
~~~~

### gdc

Instalado con:

~~~~
sudo aptitude install gdc
~~~~

### ldc

Instalado con:

~~~~
sudo aptitude install ldc
~~~~

Para poder ejecutar aplicaciones basadas en Vibed, necesitamos instalar:

~~~~
sudo apt-get install -y libssl-dev libevent-dev
~~~~

### Emacs para editar D

Instalados los siguientes paquetes desde Melpa

* d-mode
* flymake-d
* flycheck
* flycheck-dmd-dub
* flychek-d-unittest
* auto-complete (desde melpa)
* ac-dcd

Referencias
* (https://github.com/atilaneves/ac-dcd)
* (https://github.com/Hackerpilot/DCD)

## C, C++

### Instalación de Gnu Global

Para instalar las dependencias, previamente instalamos:

~~~~{shell}
sudo apt install ncurses-dev id-utils exuberant-ctags python-pygments
~~~~

Con `ctags --version` nos aseguramos de que se llama a Exuberant y no
el ctags que instala Emacs. Si no es así habrá que revisar la
definición del `PATH`

`python-pygments` no es necesario para C o C++, pero añade
funcionalidad a Global (hasta 25 lenguajes de programación más)

No podemos instalar Global desde los repos de Ubuntu, está muy
anticuado y genera bases de datos enormes y lentas. Tendremos que
compilarlo.

Nos bajamos las fuentes del programa desde [la página
oficial](https://www.gnu.org/software/global/) En el momento de
escribir esto se trata de la versión 6.6.4.

Descomprimimos los fuentes y los compilamos con:

~~~~{shell}
./configure --prefix=/usr/local --with-exuberant-ctags=/usr/bin/ctags
make
sudo make install
~~~~

He comprobado que make uninstall funciona correctamente, las librerías
quedan instaladas en `/usr/local/lib/gtags` y los ejecutables en
`/usr/local/bin`

## Rust

Instalamos siguiendo las instrucciones de
[aqui](https://www.rust-lang.org/tools/install) (Hacemos la
instalación por defecto)

~~~{bash}
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

  stable installed - rustc 1.47.0 (18bf6b4f0 2020-10-07)


Rust is installed now. Great!

To get started you need Cargo's bin directory ($HOME/.cargo/bin) in your PATH
environment variable. Next time you log in this will be done
automatically.

To configure your current shell run source $HOME/.cargo/env

~~~

Yo comento la linea del fichero `.zprofile` por que mi `.zshrc` ya lee
el fichero `.profile`

Para desinstalar `rust` bastaría con ejecutar `rustup self uninstall`

Para actualizar la instalación: `rustup update`

### RLS

Seguimos las instrucciones de la página oficial de
[LSP](https://emacs-lsp.github.io/lsp-mode/page/lsp-rust/) y de
[RLS](https://github.com/rust-lang/rls)

```bash
rustup update
rustup component add rls rust-analysis rust-src
```

### Algunas utilidades de sistema escritas en Rust

Instalamos:

```bash
cargo install tealdeer # tldr tldr
cargo install du-dust  # tldr dust
cargo install fd-find  # tldr fd
cargo install exa      # tldr exa
cargo install tokei    # tldr tokei
cargo install gitui    # gitui --help
cargo install ripgrep  # tldr rg
```


## golang

Bajamos el paquete con la última versión desde [la página oficial](https://golang.org/dl/).

Descomprimimos como _root_ en `/usr/local/`

```bash
sudo tar -C /usr/local -xvzf go1.15.7.linux-amd64.tar.gz
```

Añadimos al path con las siguientes lineas en `~/.profile`:

```bash
#golang
if [ -d "/usr/local/go" ] ; then
    export GOROOT="/usr/local/go"
    PATH="$PATH:$GOROOT/bin"
fi
if [ -d "$HOME/work/development/gocode" ] ; then
    export GOPATH="$HOME/work/development/gocode"
    PATH="$PATH:$GOPATH/bin"
fi
```

### Instalación de _gopls_ un servidor de LSP para editores:

Desde un directorio **que no sea el GOPATH**

```bash
GO111MODULE=on go get golang.org/x/tools/gopls@latest
```

### golint

```bash
go get -u golang.org/x/lint/golint
```

### Utilidades escritas en go

```bash
go install github.com/jesseduffield/lazygit@latest
```



## Processing

Bajamos el paquete de la [página web](https://processing.org/),
descomprimimimos en `~/apps/`, en las nuevas versiones incorpora un
script de instalación que ya se encarga de crear el fichero _desktop_.

La última versión incorpora varios modos de trabajo, he descargado el
modo _Python_ para probarlo.


## openFrameworks

Nos bajamos los fuentes para linux 64bits desde [la página web del
proyecto](https://openframeworks.cc), y las descomprimimos en un
directorio para proceder a compilarlas.

No hay más que seguir [las instrucciones de instalación para
linux](https://openframeworks.cc/setup/linux-install/).

La instalación no es demasiado intrusiva si tienes Ubuntu 18 o mayor y
una versión reciente del gcc.

En la primera pregunta que nos hace es necesario contestar que no. De
lo contrario falla la compilación.

Añade los siguientes paquetes a nuestro sistema

~~~~{bash}
installing OF dependencies
OF needs to install the following packages using apt-get:
curl libjack-jackd2-0 libjack-jackd2-dev freeglut3-dev libasound2-dev libxmu-dev libxxf86vm-dev g++ libgl1-mesa-dev libglu1-mesa-dev libraw1394-dev libudev-dev libdrm-dev libglew-dev libopenal-dev libsndfile-dev libfreeimage-dev libcairo2-dev libfreetype6-dev libssl-dev libpulse-dev libusb-1.0-0-dev libgtk-3-dev libopencv-dev libassimp-dev librtaudio-dev libboost-filesystem-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-libav gstreamer1.0-pulseaudio gstreamer1.0-x gstreamer1.0-plugins-bad gstreamer1.0-alsa gstreamer1.0-plugins-base gstreamer1.0-plugins-good gdb libglfw3-dev liburiparser-dev libcurl4-openssl-dev libpugixml-dev libgconf-2-4 libgtk2.0-0 libpoco-dev
Do you want to continue? [Y/n]
~~~~

No te olvides de compilar también el _Project Generator_.

## Python

De partida tenemos instalado dos versiones: _python2_ y _python3_

Parece que Linux Mint no viene con nigún python por defecto. Si
invocamos el comando `python` el sistema nos indicará que no existe.

Para escoger un python por defecto tenemos dos paquetes que podemos
instalar: `python-is-python2` y `python-is-python3`

En principio yo no quería instalar ninguno para averigüar que paquetes
no funcionaban, pero la instalación de VirtualBox hizo que se
instalara automáticamente el paquete `python-is-python2`.

~~~~{bash}
python2 -V
Python 2.7.18rc1

python3 -V
Python 3.8.2
~~~~

### Paquetes de python instalados

Son los que ya comentamos en la sección de instalación de zsh, como ya dijimos
conviene que instalemos los paquetes de desarrollo:

~~~~{bash}
sudo apt install python2-dev
sudo apt install python-all-dev
sudo apt install python3-dev
sudo apt install python3-all-dev
sudo apt install virtualenv virtualenvwrapper python3-virtualenv
~~~~

Ademas añadimos las siguientes lineas al fichero `~/.profile`:

~~~~{bash}
# WORKON_HOME for virtualenvwrapper
if [ -d "$HOME/.virtualenvs" ] ; then
WORKON_HOME="$HOME/.virtualenvs"
fi
~~~~

[Aquí](https://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html)
tenemos la referencia de comandos de _virtualenvwrapper_.

### Instalación de bpython y ptpython

[_bpython_](https://bpython-interpreter.org/) instalado desde repos `sudo apt install bpython bpython3`

[_ptpython_](https://github.com/prompt-toolkit/ptpython) instalado en un virtualenv para probarlo


### Jupyter

Una instalación para pruebas.

~~~~{bash}
mkvirtualenv -p /usr/bin/python3 jupyter
python -m pip install jupyter
~~~~

### Instalamos `python3.9`

`python3.9` está ya disponible en los repos oficiales. Para dejarla instalada:

~~~{bash}
sudo apt install python3.9 python3.9-dev python3.9-venv
~~~

### pyenv

Instalamos los pre-requisitos:

```bash
sudo apt-get update
sudo apt-get install --no-install-recommends make build-essential \
libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev \
wget curl llvm libncurses5-dev xz-utils tk-dev \
libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Podemos clonar el repo en nuestro `home`:

```bash
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

Pero también podemos añadir el _bundle_ para _Antigen_ en el fichero `.zshrc` y
dejar que se encargue de todo.

```bash
antigen bundle mattberther/zsh-pyenv
```

Añadimos al fichero `~/.profile`:

```bash
# pyenv
if [ -d "$HOME/.pyenv" ] ; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
fi
```

Y configuramos en `~/.bashrc` y en `~/.zshrc` (aunque en el último no
es necesario tocar nada si usamos el _bundle_ para _Antigen_):

```bash
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
```

Podemos probar que todo funciona con: `pyenv install -l`

Una vez instalado hay que estudiarse [la referencia de
comandos](https://github.com/pyenv/pyenv/blob/master/COMMANDS.md)

### poetry

Instalamos con:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
```

Dejamos la activación solo en el fichero `~ /.profile`, añadiendo las siguientes lineas:

```bash
# poetry
if [ -d "$HOME/.poetry"] ; then
    PATH="$HOME/.poetry/bin:$PATH"
fi
```

Las actualizaciones se realizan con `poetry self update`


## neovim

Vamos a probar _neovim_, ahora mismo la versión de los repos de Ubuntu
está actualizada a la penúltima versión (0.4.3). También podemos
descargar el appimage desde [la página
web](https://github.com/neovim/neovim/releases)

Es de esperar que alguna vez vuelvan a tener el neovim disponible en
los repos de la aplicación:

~~~~
sudo apt-add-repository ppa:neovim-ppa/stable
sudo apt update
sudo apt install neovim
~~~~

Para instalar los módulos de python creamos un _virtualev_ que más
tarde añadiremos al fichero `init.vim`.

~~~~
mkvirtualenv -p /usr/bin/python3 neovim3
sudo pip install --upgrade neovim
deactivate
~~~~

Revisar [esto](https://neovim.io/doc/user/provider.html#provider-python)


----------------------------------------------------------------------
__NOTA__: El siguiente paso ya no parece necesario, las alternativas
se han actualizado con la instalación del _neovim_.
----------------------------------------------------------------------

Para actualizar las alternativas:

~~~~
sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
sudo update-alternatives --config vi
sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
sudo update-alternatives --config vim
~~~~

#### Install _vim-plug_

Ejecutamos:

~~~~
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
~~~~

Configuramos el fichero de configuración de _nvim_
(`~/.config/nvim/init.vim`):

~~~~
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'deoplete-plugins/deoplete-jedi'

" Initialize plugin system
call plug#end()

let g:deoplete#enable_at_startup = 1

" set python enviroments
let g:python_host_prog = '/full/path/to/neovim2/bin/python'
let g:python3_host_prog = '/home/salvari/.virtualenvs/neovim3/bin/python'


~~~~

La primera vez que abramos _nvim_ tenemos que instalar los plugin porn
comando ejecutando: `:PlugInstall`

__Instalación de `dein`__


----------------------------------------------------------------------
__Nota__:
Solo hay que instalar uno de los dos o _dein_ o _plug-vim_. Yo uso
_plug-vim_ así que esto es sólo una referencia.
----------------------------------------------------------------------

<https://github.com/Shougo/dein.vim>

~~~~
" Add the dein installation directory into runtimepath
set runtimepath+=~/.config/nvim/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.config/nvim/dein')
  call dein#begin('~/.config/nvim/dein')

  call dein#add('~/.config/nvim/dein/repos/github.com/Shougo/dein.vim')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('Shougo/denite.nvim')
  if !has('nvim')
    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable
~~~~

## Firefox developer edition

El rollo de siempre, descargar desde [la página
web](https://www.mozilla.org/en-US/firefox/developer/) descomprimir en
`~/apps` y crear un lanzador.

## Navegadores cli

Herramientas útiles para depuración web

~~~~{bash}
sudo apt install httpie links
~~~~


## MariaDB

Instalamos la última estable para Ubuntu Fossa desde los repos oficiales.

Primero añadimos los repos.

Añadimos la clave de firma:

~~~~{bash}
sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
~~~~

Ahora tenemos dos opciones:

Podemos ejecutar:

~~~~
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://mariadb.mirror.liteserver.nl/repo/10.5/ubuntu focal main'
~~~~

O podemos crear un fichero `/etc/apt/apt.sources.list.d/MariaDB` con
el siguiente contenido (yo dejo las fuentes comentadas):

~~~~
# MariaDB 10.5 [Stable] repository list - created  UTC
# https://mariadb.org/download-test/
deb [arch=amd64] http://mariadb.mirror.liteserver.nl/repo/10.5/ubuntu focal main
# deb-src http://mariadb.mirror.liteserver.nl/repo/10.5/ubuntu focal main
~~~~

Y ya solo nos queda lo de siempre:

~~~~
sudo apt update
sudo apt upgrade
sudo apt install mariadb-server
~~~~

Podemos comprobar con `systemctl status mariadb`

También podemos hacer login con el usuario `root`:

~~~~
sudo mariadb -u root
~~~~

**Ojo**, hay que hacer `sudo`, el comando `mariadb -u root` no funciona.

Y ahora aseguramos la instación con:

~~~~
sudo mysql_secure_installation
~~~~

Yo diría que tienes que decir que si a todas las preguntas, excepto
quizás al _unix_socket_authentication_.

Por último sólo nos queda decidir si el servicio mariadb debe estar
ejecutándose permanentemente o no.

Si queremos pararlo y que no se arranque automáticamente al arrancar
el ordenador:

~~~~
sudo systemctl stop mariadb
sudo systemctl disable mariadb
~~~~


## Squirrel SQL Client

Bajamos el zip de estándar desde [la página web de
Squirrel](http://www.squirrelsql.org/) (yo prefiero no usar el
instalador)

Como de costumbre descomprimimos en `~/apps` y creamos una entrada en
nuestro menú de aplicaciones.

Nos descargamos también el _java connector_ para MariaDB. Desde la
página oficial. Nos interesa el fichero `maria-java-client-2.6.0.jar`

Configuramos el driver para que sepa donde está el fichero `.jar` y ya
estamos listos para trabajar.


## R y R-studio

Primero instalamos la última versión de R en nuestro pc:

~~~~{bash}
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
sudo apt update
sudo apt install r-base
~~~~

### R-studio

Ojo, tenemos _R-studio Server_ y _R-studio Desktop_, en tu portátil casi seguro que quieres el _Desktop_ y no el _Server_.

Para _R-studio Server_ descargamos la última versión disponible de _R-studio_ desde la
[página web](https://rstudio.com/products/rstudio/download-server/debian-ubuntu/)

Instalamos con _gdebi_ (basta con clicar sobre el fichero _.deb_)

Para _R-studio Desktop_ descargamos la última versión disponible desde la [página web](https://www.rstudio.com/products/rstudio/download/#download)

Igual que el _Server_ instalamos el paquete del _Desktop_ con _gedebi_.

## Octave

Instalado desde flatpak

~~~~
sudo flatpak install flathub org.octave.Octave
~~~~
