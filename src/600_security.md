# Seguridad

## Autenticación en servidores por clave pública

Generar contraseñas para conexión servidores remotos:

~~~~
cd ~
ssh-keygen -b 4096 [-t dsa | ecdsa | ed25519 | rsa | rsa1]
cat .ssh/
~~~~

Solo resta añadir nuestra clave pública en el fichero
`authorized_keys` del servidor remoto.

~~~~
cat ~/.ssh/id_xxx.pub | ssh user@hostname 'cat >> .ssh/authorized_keys'
~~~~

[¿Cómo funciona esto?](https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process)

## Claves gpg

`gpg --gen-key` Para generar nuestra clave.

* __Siempre__ hay que ponerle una fecha de expiración, la puedes cambiar más tarde.
* __Siempre__ hay que escoger la máxima longitud posible

## Seahorse

Para manejar todas nuestras claves con comodidad:

 `sudo apt install seahorse`

## Conexión a github con claves ssh

Usando este método podemos conectarnos a github sin tener que teclear
la contraseña en cada conexión.

### Claves ssh

Podemos echar un ojo a nuestras claves desde `seahorse` la aplicación
de gestión de claves que hemos instalado. También podemos ver las
claves que tenemos generadas:

~~~
ls -al ~/.ssh
~~~

En las claves listadas nuestras claves públicas aparecerán con
extensión `.pub`

También podemos comprobar que claves hemos añadido ya a nuestro agente
ssh con:

~~~
ssh-add -l
~~~

Para generar una nueva pareja de claves ssh:

~~~
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
~~~

Podremos dar un nombre distintivo a los ficheros de claves generados y
poner una contraseña adecuada a la clave. Si algún dia queremos
cambiar la contraseña:

~~~
ssh-keygen -p
~~~

Ahora tenemos que añadir nuestra clave ssh en nuestra cuenta de
github, para ello editamos con nuestro editor de texto favorito el
fichero `~/.ssh/id_rsa.pub` y copiamos el contenido integro. Después
pegamos ese contenido en el cuadro de texto de la web de github.

Para comprobar que las claves instaladas en github funcionan
correctamente:

~~~~
ssh -T git@github.com
Hi salvari! You've successfully authenticated, but GitHub does not provide shell access.
~~~~

Este mensaje indica que todo ha ido bien.

Ahora en los repos donde queramos usar ssh debemos cambiar el remote:

~~~~
git remote set-url origin git@github.com:$USER/$REPONAME.git
~~~~

## Signal

El procedimiento recomendado en la página oficial:

~~~~bash
curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update && sudo apt install signal-desktop
~~~~

------------

__NOTA__: Parece que no funciona. Lo he instalado via _flatpack_

------------

## Element (cliente de matrix.org)

Instalamos con:

```bash
sudo apt install -y wget apt-transport-https

sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list

sudo apt update

sudo apt install element-desktop
```

## Lector DNI electrónico

Descargamos la aplicación en formato `.deb` desde [la página de
descargas del portal
DNIe](https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1112).

Automáticamente nos instalará las dependecias: `libccid`, `pcsd` y `pinentry-gtk2`. A mayores instalamos:

~~~~bash
sudo apt-get install pcsc-tools opensc
~~~~

El opensc no es necesario para el DNIe, pero nos permite usar otras tarjetas.

Como root ejecutamos pcsc_scan:

~~~~
root@rasalhague:~# pcsc_scan
PC/SC device scanner
V 1.4.23 (c) 2001-2011, Ludovic Rousseau <ludovic.rousseau@free.fr>
Compiled with PC/SC lite version: 1.8.11
Using reader plug'n play mechanism
Scanning present readers...
Waiting for the first reader...
~~~~

Si insertamos el lector veremos algo como esto:

~~~~
root@rasalhague:~# pcsc_scan
PC/SC device scanner
V 1.4.23 (c) 2001-2011, Ludovic Rousseau <ludovic.rousseau@free.fr>
Compiled with PC/SC lite version: 1.8.11
Using reader plug'n play mechanism
Scanning present readers...
Waiting for the first reader...found one
Scanning present readers...
0: C3PO LTC31 v2 (11061005) 00 00

Wed Jan 25 01:17:20 2017
Reader 0: C3PO LTC31 v2 (11061005) 00 00
  Card state: Card removed,
~~~~

Si insertamos un DNI veremos que se lee la información de la tarjeta insertada:

~~~~
Reader 0: C3PO LTC31 v2 (11061005) 00 00
  Card state: Card inserted,
~~~~

y mas rollo


Para abrir los certificados en el navegador Firefox, nos lo explica [esta página de la AEAT](https://www.agenciatributaria.es/AEAT.internet/Inicio/Ayuda/_comp_Consultas_informaticas/Categorias/Firma_digital__certificado_o_DNIe__y_sistema_Cl_ve_PIN/DNI_electronico__DNIe_/Comprobaciones_tecnicas_para_el_funcionamiento_del_DNIe/Comprobaciones_tecnicas_del_DNIe_con_Mozilla_Firefox_y_Linux/Comprobaciones_tecnicas_del_DNIe_con_Mozilla_Firefox_y_Linux.shtml)

Como se puede ver el link de la AEAT, los pasos necesarios para Firefox son:

1. Vamos a preferencias y buscamos 'cert'
2. En el diálogo de certificados abrimos los `Dispositivos de Seguridad` (_Security Devices_)
3. Para dar de alta un nuevo dispositivo pulsamos el botón `Cargar` (_Load_)
4. Damos un nombre (p.ej. `DNIe`) y asociamos el driver: `/usr/lib/libpkcs11-dnie.so`
5. Adicionalmente podemos `Cargar` (crear), otro dispositivo con el
   driver `opensc`, no es necesario para el DNIe pero nos añade
   soporte para otras tarjetas. (Nombre: OtrasTarjetas, Driver: `/usr/lib/x86_64-linux-gnu/pkcs11/opensc-pkcs11.so)

----

**NOTA**:

Para cada tarjeta puede hacer falta un driver diferente, tendrás que
investigar con ayuda del `pcsc_scan` y herramientas similares.

----
